﻿using UnityEngine;
using ActionCode.UI;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.Events;
using System.Collections.Generic;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// UI Pause Menu component. 
    /// You can <see cref="Pause"/>, <see cref="Resume"/> or <see cref="Restart"/> your game.
    /// </summary>
    public class PauseMenu : AbstractMenu
    {
        public AudioMixer audioMixer;
        public string musicVolumeParam = "musicVolume";
        public string sfxVolumeParam = "sfxVolume";

        [Header("Buttons")]
        public UIPanel warningPanel;
        public Button pauseButton;
        public Toggle musicToggle;
        public Toggle sfxToggle;

        /// <summary>Callback invoked when <see cref="Show"/> function is fired.</summary>
        [Header("Events")]
        public UnityEvent onOpenEvent;
        /// <summary>Callback invoked when <see cref="Hide"/> function is fired.</summary>
        public UnityEvent onCloseEvent;

        public bool Paused { get; protected set; }

        /// <summary>Set this value to enable/disable background music.</summary>
        public bool MusicEnabled
        {
            get { return _musicVolumeOn; }
            set
            {
                _musicVolumeOn = value;
                SetAudioMixerExposedParam(musicVolumeParam, _musicVolumeOn ? 0f : -80f);
            }
        }
        /// <summary>Set this value to enable/disable sound effects music.</summary>
        public bool SFXEnabled
        {
            get { return _sfxVolumeOn; }
            set
            {
                _sfxVolumeOn = value;
                SetAudioMixerExposedParam(sfxVolumeParam, _sfxVolumeOn ? 0f : -80f);
            }
        }

        private bool _musicVolumeOn = true;
        private bool _sfxVolumeOn = true;
        private float _lastTimeScale = 0f;
        private List<IPauseable> _pausedComponents = new List<IPauseable>();

        protected override void Reset()
        {
            base.Reset();
            warningPanel = transform.GetComponentInChildren<UIPanel>(true);
            pauseButton = transform.Find<Button>("PauseButton");
        }

        /// <summary>
        /// Shows this GameObject by enabling its Canvas component and calls onOpenEvent callback.
        /// </summary>
        public override void Show()
        {
            CheckToggles();
            onOpenEvent.Invoke();
            base.Show();
        }

        /// <summary>
        /// Hides this GameObject by disabling its Canvas component and calls onCloseEvent callback.
        /// </summary>
        public override void Hide()
        {
            onCloseEvent.Invoke();
            base.Hide();
        }

        /// <summary>
        /// Register a neu pauseable component.
        /// </summary>
        /// <param name="pauseable">This component will be paused when <see cref="Pause"/> is called.</param>
        public void RegisterComponent(IPauseable pauseable)
        {
            _pausedComponents.Add(pauseable);
        }

        /// <summary>
        /// Finds and pauses all active components implementing IPauseable interface. 
        /// Set timeScale to 0.
        /// </summary>
        public virtual void Pause()
        {
            Show();
            Paused = true;
            if (pauseButton) pauseButton.gameObject.SetActive(false);
            foreach (IPauseable comp in _pausedComponents) comp.Pause();

            _lastTimeScale = Time.timeScale;
            Time.timeScale = 0f;
        }

        /// <summary>
        /// Unpauses all components paused by <see cref="Pause"/> function.
        /// Set timeScale to the value set before enter on pause.
        /// </summary>
        public virtual void Resume()
        {
            Hide();
            Paused = false;
            if (pauseButton) pauseButton.gameObject.SetActive(true);

            foreach (IPauseable comp in _pausedComponents) comp.Resume();
            Time.timeScale = _lastTimeScale;
        }

        /// <summary>
        /// Hides this Pause Menu so player is able to clearly see gameplay and starts countdown.
        /// Unpauses all components paused by <see cref="Pause"/> function when the given countdown finishes.
        /// </summary>
        /// <param name="countdown">CountdownTimer component used to display time until the games resumes.</param>
        public void Resume(CountdownTimer countdown)
        {
            Hide();
            countdown.StartCountdown(Resume);
        }

        /// <summary>
        /// Finds and restarts all active components implementing IRestartable interface.
        /// </summary>
        public void Restart()
        {
            Resume();
            IRestartable[] components = transform.FindAll<IRestartable>();
            foreach (IRestartable comp in components) comp.Restart();
        }

        /// <summary>
        /// Shows the Warning Panel component if avaliable.
        /// </summary>
        public void ShowWarningPanel()
        {
            if (warningPanel) warningPanel.Show();
        }

        /// <summary>
        /// Toggles the music volume by iverting <see cref="MusicEnabled"/>.
        /// </summary>
        public void ToggleMusic()
        {
            MusicEnabled = !MusicEnabled;
        }

        /// <summary>
        /// Toggles the sounds effects volume by iverting <see cref="SFXEnabled"/>.
        /// </summary>
        public void ToggleSFX()
        {
            SFXEnabled = !SFXEnabled;
        }

        protected override void OnBackButtonPressed()
        {
            if (Hidden) Pause();
            else Resume();
        }

        private void CheckToggles()
        {
            if (musicToggle) musicToggle.isOn = MusicEnabled;
            if (sfxToggle) sfxToggle.isOn = SFXEnabled;
        }

        /// <summary>
        /// Set the given Audio Mixer component parameter with the given float value.
        /// </summary>
        /// <param name="exposedParam">The name of the parameter present on Audio Mixer.</param>
        /// <param name="value">The value to set on.</param>
        public void SetAudioMixerExposedParam(string exposedParam, float value)
        {
            if (audioMixer) audioMixer.SetFloat(exposedParam, value);
        }
    }
}