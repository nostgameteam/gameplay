﻿using UnityEngine;
using ActionCode.UI;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace ActionCode.Gameplay
{
    [RequireComponent(typeof(GridLayoutGroup))]
    public class LevelMenu : AbstractMenu
    {
        public string levelPrefix = "Level-";

        private void Start()
        {
            BindButtonActionEvents();
        }

        /// <summary>
        /// Set timeScale to 1f and loads the given scene.
        /// </summary>
        /// <param name="sceneName">Name of the Scene  load.</param>
        public virtual void LoadScene(string sceneName)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneName);
        }

        private void BindButtonActionEvents()
        {
            Button[] levelButtons = GetComponentsInChildren<Button>();
            foreach (Button button in levelButtons)
            {
                Text textComponent = button.GetComponentInChildren<Text>();
                if (textComponent)
                {
                    string currentLevelName = levelPrefix + textComponent.text;
                    button.onClick.AddListener(delegate { LoadScene(currentLevelName); });
                }
            }
        }
    }
}