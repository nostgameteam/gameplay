﻿using UnityEngine;
using ActionCode.UI;
using UnityEngine.SceneManagement;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component is used to display a Main Menu containing a <see cref="UIPanel"/> used when the user pressed the Escape key.
    /// <para>On Awake, the QuitGame action will be binded to the Yes button action  from WarningPanel if none was.</para>
    /// </summary>
    public class MainMenu : AbstractMenu
    {
        public UIPanel warningPanel;

        protected override void Reset()
        {
            base.Reset();
            warningPanel = transform.FindChildComponent<UIPanel>("QuitWarningPanel", false);
        }
        protected override void Awake()
        {
            base.Awake();
            SetupWarningPanel();
        }

        /// <summary>
        /// Set timeScale to 1f and loads the given scene.
        /// </summary>
        /// <param name="sceneName">Name of the Scene  load.</param>
        public virtual void LoadScene(string sceneName)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(sceneName);
        }

        /// <summary>
        /// Set timeScale to 1f and quits the game.
        /// </summary>
        public virtual void QuitGame()
        {
            Time.timeScale = 1f;
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public override void Hide()
        {
            base.Hide();
            warningPanel.Hide();
        }

        protected override void OnBackButtonPressed()
        {
            if (warningPanel)
            {
                if (warningPanel.Hidden) warningPanel.Show();
                else warningPanel.Hide();
            }
        }
        
        protected virtual void SetupWarningPanel()
        {
            if (warningPanel && !warningPanel.IsActionBindedToYesButton())
                warningPanel.SetYesClickAction(QuitGame);
        }
    }
}