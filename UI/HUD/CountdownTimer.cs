﻿using UnityEngine;
using UnityEngine.UI;
using ActionCode.UI;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component should be used to provide a countdown timer. Countdown Text can be animated using Animator.
    /// An <see cref="UnityEvent"/> will be triggered when countdown finishes.
    /// <para>This component requires a <see cref="Canvas"/> component for optimization purposes.
    /// Since the countdown behaviour only happens for a brief amount of time, it's more performative
    /// to use this component with a nested Canvas (a Canvas placed as a child of another) and disable it when not necessary.</para>
    /// <para>Nested Canvases isolate their children from their parent so a dirty child will not force a 
    /// parent Canvas to rebuild its geometry. Therefore, some batches are saved and performance increases.</para>
    /// <para>IMPORTANT: Set a trigger paramenter inside the Animator component with 'countdown' name. 
    /// Each time countdown decreases one second, this trigger will be called.</para>
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public sealed class CountdownTimer : CanvasDrawer
    {
        [Min(0f)] public int countdownTime = 3;
        public Text countdownText;
        public AudioSource audioSource;

        /// <summary>Callback event triggered when countdown finishes.</summary>
        [Space]
        public UnityEvent onCountdownFinish;

        private readonly int TRIGGER_PARAM = Animator.StringToHash("countdown");

        protected override void Reset()
        {
            base.Reset();
            audioSource = GetComponent<AudioSource>();
            countdownText = transform.FindChildComponent<Text>("CountdownText");
            countdownText.enabled = true;

            audioSource.playOnAwake = audioSource.loop = false;
            audioSource.spatialBlend = 0f;
        }

        /// <summary>
        /// Starts the countdown with the given countdown finish action and other values set on Inspector.
        /// Enable this GameObject if not.
        /// </summary>
        /// <param name="countdownFinishAction">Action to be triggered when countdown finishes.</param>
        public void StartCountdown(UnityAction countdownFinishAction)
        {
            onCountdownFinish.RemoveAllListeners();
            onCountdownFinish.AddListener(countdownFinishAction);
            StartCountdown();
        }

        /// <summary>
        /// Starts the countdown with the given time and countdown finish action.
        /// Enable this GameObject if not.
        /// </summary>
        /// <param name="time">Amount of time to countdown.</param>
        /// <param name="countdownFinishAction">Action to be triggered when countdown finishes.</param>
        public void StartCountdown(int time, UnityAction countdownFinishAction)
        {
            onCountdownFinish.RemoveAllListeners();
            onCountdownFinish.AddListener(countdownFinishAction);
            StartCountdown(time);
        }

        /// <summary>
        /// Starts the countdown with the given time and other values set on Inspector.
        /// Enable this GameObject if not.
        /// </summary>
        /// <param name="time">Amount of time to countdown.</param>
        public void StartCountdown(int time)
        {
            countdownTime = time;
            StartCountdown();
        }

        /// <summary>
        /// Starts the countdown with values set on Inspector.
        /// Enable this GameObject if not.
        /// </summary>
        public void StartCountdown()
        {
            Show();
            StopAllCoroutines();
            StartCoroutine(StartCountdownCoroutine());
        }

        private System.Collections.IEnumerator StartCountdownCoroutine()
        {
            int currentTime = countdownTime;

            while (currentTime > 0)
            {
                if (audioSource.clip) audioSource.Play();
                countdownText.text = currentTime.ToString();
                animator.SetTrigger(TRIGGER_PARAM);
                yield return new WaitForSecondsRealtime(1f);
                currentTime -= 1;
            }

            onCountdownFinish.Invoke();
            Hide();
        }
    }
}