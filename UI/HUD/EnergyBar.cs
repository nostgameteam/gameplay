﻿using UnityEngine;
using ActionCode.UI;
using UnityEngine.UI;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// UI Energy bar component.
    /// </summary>
    public class EnergyBar : AbstractUIDrawer, IRestartable
    {
        [Min(0f)] public float initialValue = 100f;
        [Min(0f)] public float maxValue = 100f;
        public AudioSource audioSource;

        public Image background;
        public Image fillground;
        public Image foreground;

        public virtual float Value
        {
            get { return value; }
            set
            {
                this.value = Mathf.Clamp(value, 0f, maxValue);
                UpdateFillground();
                if (audioSource) audioSource.Play();
            }
        }
        protected float value = 0;

        protected override void Reset()
        {
            base.Reset();
            background = transform.FindChildComponent<Image>("Background");
            fillground = background.transform.FindChildComponent<Image>("Fillground");
            foreground = transform.FindChildComponent<Image>("Foreground", false);

            fillground.type = Image.Type.Filled;
            fillground.fillMethod = Image.FillMethod.Horizontal;
            fillground.fillOrigin = 0;
        }
        private void Awake()
        {
            Restart();
        }
        private void OnValidate()
        {
            if (fillground)
            {
                fillground.fillAmount = initialValue / maxValue;
            }
        }

        protected virtual void UpdateFillground()
        {
            fillground.fillAmount = value / maxValue;
        }

        public virtual void Restart()
        {
            Value = initialValue;
        }
        public virtual bool IsFull()
        {
            return Value == maxValue;
        }
    }
}