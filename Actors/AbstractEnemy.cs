using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Damager))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(Damageable))]
    public abstract class AbstractEnemy : MonoBehaviour
    {
        public float attackInterval = 5f;
        public FireWeapon weapon;

        [SerializeField] protected AbstractCollisionDetector collisionDetector;
        [SerializeField] protected Animator animator;

        private void Reset()
        {
            animator = GetComponent<Animator>();
            collisionDetector = GetComponentInChildren<AbstractCollisionDetector>(true);
            weapon = GetComponentInChildren<FireWeapon>(true);

            Damager damager = GetComponent<Damager>();
            Damageable damageable = GetComponent<Damageable>();

            damager.damage = GetHitDamage();
            damager.targetLayers = GameObjectExtention.NameToLayer("Player");

            damageable.inicialHealth = GetInicialHealth();
            damageable.invulnerabilityDuration = GetInvulnerabilityDuration();

            if (collisionDetector) collisionDetector.targetLayers = damager.targetLayers;
            ResetComponents();
        }
        private void Start()
        {
            if (collisionDetector)
            {
                collisionDetector.onEnterEvent = new UnityEvent();
                collisionDetector.onStayEvent = new UnityEvent();
                collisionDetector.onExitEvent = new UnityEvent();
                BindCollisionDetectionEvents();
            }
            InitAnimatorParamsId();
        }

        public virtual void Attack() { }
        public virtual void Turn() { }
        public virtual void Destroy() { }

        public abstract float GetInicialHealth();
        public abstract float GetInvulnerabilityDuration();
        public abstract float GetHitDamage();
        protected abstract void ResetComponents();
        protected abstract void BindCollisionDetectionEvents();
        protected abstract void InitAnimatorParamsId();
    }
}