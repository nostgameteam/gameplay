﻿namespace ActionCode.Gameplay
{
    /// <summary>
    /// Interface used for objects able to perform smt when the game pause/resume
    /// </summary>
    public interface IPauseable
    {
        void Pause();
        void Resume();
    }
}