using UnityEngine;

namespace ActionCode.Gameplay
{
    [System.Serializable]
    public class Vector3Curve
    {
        public AnimationCurve x = AnimationCurve.Linear(0f, 1f, 1f, 1f);
        public AnimationCurve y = AnimationCurve.Linear(0f, 1f, 1f, 1f);
        public AnimationCurve z = AnimationCurve.Linear(0f, 1f, 1f, 1f);

        public void SetLinearAxis(float inicialValue = 0f, float finalValue = 0f)
        {
            x = AnimationCurve.Linear(0f, inicialValue, 1f, finalValue);
            y = AnimationCurve.Linear(0f, inicialValue, 1f, finalValue);
            z = AnimationCurve.Linear(0f, inicialValue, 1f, finalValue);
        }

        public AnimationCurve CreateSinCurve(float amplitude = 1f, float finalTime = 1f)
        {
            float deltaTime = finalTime / 4f;
            AnimationCurve curve = new AnimationCurve();
            curve.AddKey(0f, 0f);
            curve.AddKey(deltaTime, amplitude);
            curve.AddKey(finalTime / 2f, 0f);
            curve.AddKey(finalTime - deltaTime, -amplitude);
            curve.AddKey(finalTime, 0f);

            for (int i = 0; i < curve.keys.Length; i++)
            {
                curve.SmoothTangents(i, 0f);
            }

            return curve;
        }

        public Vector3 GetValues()
        {
            return GetValues(Time.timeSinceLevelLoad);
        }
        public Vector3 GetValues(float time)
        {
            return new Vector3(
                x.Evaluate(time),
                y.Evaluate(time),
                z.Evaluate(time));
        }
    }
}