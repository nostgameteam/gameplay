﻿using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// Abstract class to provide animation behaviour.
    /// </summary>
    public abstract class AbstractAnimation : MonoBehaviour, IPauseable
    {
        [Tooltip("Speed of the animation.")]
        public float speed = 1f;

        /// <summary>This event is triggered when the animation is resumed.</summary>
        [Header("Events")]
        public UnityEvent onResumeAnimation;
        /// <summary>This event is triggered when the animation is paused.</summary>
        public UnityEvent onPauseAnimation;
        /// <summary>This event is triggered when the animation is stoped.</summary>
        public UnityEvent onStopAnimation;
        
        /// <summary>Current time of the animation.</summary>
        public float CurrentTime { get; protected set; }

        private void Reset()
        {
            ResetComponents();
        }
        private void Update()
        {
            CurrentTime += speed * Time.deltaTime;
            UpdateAnimation();
        }

        /// <summary>Resumes the animation. Invokes onResumeAnimation events.</summary>
        public virtual void Resume()
        {
            onResumeAnimation.Invoke();
            this.enabled = true;
        }
        /// <summary>Pauses the animation. Invokes onPauseAnimation events.</summary>
        public virtual void Pause()
        {
            onPauseAnimation.Invoke();
            this.enabled = false;
        }
        /// <summary>Stops the animation. Invokes onStopAnimation events.</summary>
        public virtual void Stop()
        {
            onStopAnimation.Invoke();
            CurrentTime = 0f;
            UpdateAnimation();
            Pause();
        }
        /// <summary>
        /// Replays the animation by calling <see cref="Stop"/> and <see cref="Resume"/> methods. 
        /// <para>It also ivokes onStopAnimation and onResumeAnimation events.</para>
        /// </summary>
        public virtual void Replay()
        {
            Stop();
            Resume();
        }

        /// <summary>This function is called on <see cref="Reset"/> method.</summary>
        protected abstract void ResetComponents();
        /// <summary>This function is called on <see cref="Update"/> method.</summary>
        protected abstract void UpdateAnimation();
    }
}