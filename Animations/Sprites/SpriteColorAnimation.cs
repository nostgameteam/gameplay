﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component animates <see cref="SpriteRenderer.color"/> by the given <see cref="gradient"/>.
    /// </summary>
    public sealed class SpriteColorAnimation : AbstractAnimation
    {
        [Tooltip("Sprites color will be animated using this gradient.")]
        public Gradient gradient;
        [Tooltip("Defines how to wrap the animation.")]
        public WrapMode wrapMode = WrapMode.Loop;
        [Tooltip("Sprites renderers that will be animated.")]
        public SpriteRenderer[] renderers;

        /// <summary>The current color based on the <see cref="gradient"/>.</summary>
        public Color CurrentColor { get; private set; }
        /// <summary>The <see cref="gradient"/> start color.</summary>
        public Color StartColor
        {
            get { return gradient.colorKeys[0].color; }
            set { gradient.colorKeys[0].color = value; }
        }
        /// <summary>The <see cref="gradient"/> end color.</summary>
        public Color EndColor
        {
            get { return gradient.colorKeys[gradient.colorKeys.Length - 1].color; }
            set { gradient.colorKeys[gradient.colorKeys.Length - 1].color = value; }
        }

        private Color[] _originalColors;

        private void Awake()
        {
            // Initialize original colors
            _originalColors = new Color[renderers.Length];
            for (int i = 0; i < _originalColors.Length; i++)
            {
                _originalColors[i] = renderers[i].color;
            }
        }

        protected override void ResetComponents()
        {
            renderers = GetComponentsInChildren<SpriteRenderer>();
        }
        protected override void UpdateAnimation()
        {
            switch (wrapMode)
            {
                case WrapMode.Default:
                case WrapMode.Once:
                    if (CurrentTime > 1) Stop();
                    break;

                case WrapMode.Loop:
                    if (CurrentTime > 1f) CurrentTime = 0f;
                    //CurrentTime = Mathf.Repeat(CurrentTime, 1f);
                    break;

                case WrapMode.PingPong:
                    CurrentTime = Mathf.PingPong(Time.time * speed, 1f);
                    break;

                case WrapMode.ClampForever:
                    CurrentTime = Mathf.Clamp01(CurrentTime);
                    break;
            }

            CurrentColor = gradient.Evaluate(CurrentTime);
            SetRenderersColor(CurrentColor);
        }
        public override void Stop()
        {
            base.Stop();
            SetRenderersOriginalColors();
        }

        /// <summary>
        /// Set all Sprite Renderers colors to the given color.
        /// </summary>
        /// <param name="color"></param>
        public void SetRenderersColor(Color color)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].color = color;
            }
        }
        /// <summary>Set the renderers to its original colors.</summary>
        public void SetRenderersOriginalColors()
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].color = _originalColors[i];
            }
        }

        /// <summary>Setups this component to a Blink animation.</summary>
        [ContextMenu("Setup Blink Animation")]
        public void SetupBlinkAnimation()
        {
            GradientColorKey[] colorKeys = new GradientColorKey[2];
            GradientAlphaKey[] alphaKeys = new GradientAlphaKey[2];

            colorKeys[0].color = colorKeys[1].color = Color.white;
            colorKeys[0].time = 0f;
            colorKeys[1].time = 1f;

            alphaKeys[0].alpha = 1f;
            alphaKeys[0].time = 0f;
            alphaKeys[1].alpha = 0f;
            alphaKeys[1].time = 1f;

            if (gradient == null) gradient = new Gradient();
            gradient.SetKeys(colorKeys, alphaKeys);
        }
    }
}