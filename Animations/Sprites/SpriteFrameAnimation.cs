﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component animates <see cref="SpriteRenderer.sprite"/> by the given <see cref="frames"/>.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class SpriteFrameAnimation : AbstractAnimation
    {
        [Tooltip("Sprite renderer that will be animated.")]
        public SpriteRenderer spriteRenderer;
        [Tooltip("Animation Curve for sprite frames.")]
        public AnimationCurve curve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
        [Tooltip("Sprite frames that will animate the SpriteRenderer.")]
        public Sprite[] frames;

        protected override void ResetComponents()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            frames = new Sprite[1] { spriteRenderer.sprite };
        }
        protected override void UpdateAnimation()
        {
            int index = Mathf.FloorToInt(curve.Evaluate(CurrentTime));
            if (index > frames.Length - 1) index = frames.Length - 1;
            spriteRenderer.sprite = frames[index];
        }

        /// <summary>Creates a linear curve between 0 and the number of sprites frames.</summary>
        [ContextMenu("Create Linear Curve")]
        private void CreateLinearCurve()
        {
            curve = AnimationCurve.Linear(0f, 0f, 1f, frames.Length);
        }
    }
}