﻿using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component is used to triggers collisions events with <see cref="Damageable"/> components.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class Damager : MonoBehaviour
    {        
        [Tooltip("Amount of damage to inflict."), Min(0f)]
        public float damage = 1f;
        [Tooltip("Offset of the bounding box using for detect collisions.")]
        public Vector3 offset = Vector3.zero;
        [Tooltip("Size of the bounding box using for detect collisions.")]
        public Vector3 size = Vector3.one;
        [Tooltip("Layer Mask used to narrow the bounding box collision.")]
        public LayerMask targetLayers;
        [Tooltip("Enable this to inflict damage even if the damageable component is on invincible.")]
        public bool ignoreInvincibility = false;
        [Tooltip("Enable this with you are developing a 2D game.")]
        public bool use2DCollider = false;

        /// <summary>Callback event triggered when the TriggerHit event happens.</summary>
        [Header("Events")]
        public DamagerEvent onDamageEvent;
        /// <summary>Callback event triggered when a hit happens with any collider.</summary>
        public UnityEvent onHitEvent;

        private void Reset()
        {
            Collider2D collider2D = GetComponent<Collider2D>();
            if (collider2D)
            {
                offset = collider2D.offset;
                size = collider2D.bounds.size;
            }
            else
            {
                Collider collider = GetComponent<Collider>();
                if (collider)
                {
                    offset = collider.bounds.center;
                    size = collider.bounds.size;
                }
            }
        }
        private void FixedUpdate()
        {
            if (use2DCollider)
            {
                Collider2D collider = Physics2D.OverlapBox(transform.position + offset, size, 0f, targetLayers);
                if (collider)
                {
                    Damageable damageableComponent = collider.GetComponent<Damageable>();
                    if (damageableComponent) TriggerHit(damageableComponent);
                    onHitEvent.Invoke();
                }
            }
            else
            {
                Collider[] colliders = Physics.OverlapBox(transform.position + offset, size / 2f, transform.rotation, targetLayers);
                if (colliders.Length > 0)
                {
                    Damageable damageableComponent = colliders[0].GetComponent<Damageable>();
                    if (damageableComponent) TriggerHit(damageableComponent);
                    onHitEvent.Invoke();
                }
            }
        }

        /// <summary>
        /// Triggers the collision with a given Damageable component.
        /// <para>Invokes this <see cref="onDamageEvent"/> callback and <see cref="Damageable.TakeDamage(Damager, bool)"/>.</para>
        /// </summary>
        /// <param name="Damageable">The Damageable component that will take damage.</param>
        public void TriggerHit(Damageable damageable)
        {
            damageable.TakeDamage(this, ignoreInvincibility);
            onDamageEvent.Invoke(this);
        }        
    }
    /// <summary>
    /// A custom UnityEvent class for providing Damager events.
    /// </summary>
    [System.Serializable] public class DamagerEvent : UnityEvent<Damager> { }
}