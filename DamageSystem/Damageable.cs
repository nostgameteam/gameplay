﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component is used to receives damage events from <see cref="Damager"/> components.
    /// <para>You may just attach it to a GameObject and use rightway to provide a damage events.</para>
    /// <para>To work correctily, you need to instanciate a <see cref="Damager"/> component into 
    /// your scene to be able to trigger the collision using <see cref="Damager.TriggerHit(Damageable)"/>.</para>
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class Damageable : MonoBehaviour
    {
        [Tooltip("Maximum amount of energy possible."), Min(0f)]
        public float maximumHealth = 10f;
        [Min(0f), Tooltip("Inicial amount of energy.")]
        public float inicialHealth = 10f;
        [Min(0f), Tooltip("It won't receive damage until this time ends.")]
        public float invulnerabilityDuration = 1f;
        [Tooltip("Animation to play when invulnerable."), ContextMenuItem("Add Shine Animation", "SetupShineAnimation")]
        public SpriteColorAnimation shineAnimation;

        /// <summary>Callback event triggered when the TakeDamage event happens.</summary>
        [Header("Events")]
        public DamageableEvent onTakeDamageEvent;
        /// <summary>Callback event triggered when the SetHealth event happens.</summary>
        public DamageableEvent onSetHealthEvent;
        /// <summary>Callback event triggered when the RemoveHealth event happens and there is no more health left.</summary>
        public DamageableEvent onDieEvent;
        /// <summary>Callback event triggered every time a TakeDamage event happens and Invulnerable is true.</summary>
        public DamagerEvent onInvulnerableEvent;

        public float CurrentHealth { get; private set; }
        public bool Invulnerable { get; private set; }

        private void Reset()
        {
            shineAnimation = GetComponent<SpriteColorAnimation>();
            if (GetComponent<Collider2D>() == null && GetComponent<Collider>() == null)
                Debug.LogError("You should include a Collider2D or Collider on this component to detect damage collisions.");
        }
        private void Start()
        {
            CurrentHealth = inicialHealth;
            Invulnerable = false;
        }

        /// <summary>
        /// Adds a <see cref="SpriteColorAnimation"/> component on this 
        /// GameObject if there is none and set it up properly.
        /// </summary>
        public void SetupShineAnimation()
        {
            if (shineAnimation == null)
            {
                shineAnimation = gameObject.AddComponent<SpriteColorAnimation>();
                shineAnimation.SetupBlinkAnimation();
                shineAnimation.enabled = false;
            }
        }

        /// <summary>
        /// Removes health and invokes the onDamageTakenEvent callback if this component is enabled and not invulnerable.
        /// </summary>
        /// <param name="damager">The damager this component will take.</param>
        /// <param name="ignoreInvulnerability">If true, will ignore invulnerability and will force to inflict damage.</param>
        public void TakeDamage(Damager damager, bool ignoreInvulnerability = false)
        {
            if (!this.enabled) return;
            if (Invulnerable && !ignoreInvulnerability)
            {
                onInvulnerableEvent.Invoke(damager);
                return;
            }

            onTakeDamageEvent.Invoke(this);
            RemoveHealth(damager.damage);
        }

        /// <summary>
        /// Adds amount to Current Health and checks if the total health is not bigger than maximum health.
        /// Also invokes the onSetHealthEvent callback.
        /// </summary>
        /// <param name="amount">The amount to add on Current Health.</param>
        public void AddHealth(int amount)
        {
            float health = CurrentHealth + Mathf.Abs(amount);
            if (health > maximumHealth) health = maximumHealth;
            SetHealth(health);
        }

        /// <summary>
        /// Removes amount to Current Health and start invulnerability.
        /// Also invokes the onSetHealthEvent callback and, if health is smaller than 0f, 
        /// invokes onDieEvent callback and disable this component.
        /// </summary>
        /// <param name="amount">The amount to remove on Current Health.</param>
        public void RemoveHealth(float amount)
        {
            float health = CurrentHealth - Mathf.Abs(amount);
            if (health < 0.001f)
            {
                health = 0f;
                onDieEvent.Invoke(this);
                this.enabled = false;
            }
            else if (invulnerabilityDuration > 0.01f)
            {
                MakeInvulnerable();
            }

            SetHealth(health);
        }

        /// <summary>
        /// Set amount to health and invokes the onSetHealthEvent callback.
        /// </summary>
        /// <param name="amount"></param>
        public void SetHealth(float amount)
        {
            CurrentHealth = amount;
            onSetHealthEvent.Invoke(this);
        }

        /// <summary>
        /// Set Current Health to maximum.
        /// </summary>
        public void SetMaximunHealth()
        {
            CurrentHealth = maximumHealth;
        }

        /// <summary>
        /// Starts invulnerability.
        /// </summary>
        public void MakeInvulnerable()
        {
            StopAllCoroutines();
            StartCoroutine(InvunerabilityCoroutine());
        }

        /// <summary>
        /// Starts invulnerability and set its duration.
        /// </summary>
        /// <param name="duration">The duration invulnerability will have.</param>
        public void MakeInvulnerable(float duration)
        {
            invulnerabilityDuration = duration;
            MakeInvulnerable();
        }

        private IEnumerator InvunerabilityCoroutine()
        {
            Invulnerable = true;
            if (shineAnimation) shineAnimation.Resume();
            yield return new WaitForSeconds(invulnerabilityDuration);
            Invulnerable = false;
            if (shineAnimation) shineAnimation.Stop();
        }

        /// <summary>
        /// A custom UnityEvent class for providing Damageable events.
        /// </summary>
        [System.Serializable] public class DamageableEvent : UnityEvent<Damageable> { }
    }
}