# Gameplay

## Package Deprecated - proccess of decoupling code into small packages.

### What is this package for? ###
* This Unity package is designed for gameplay components such as Pool System, Damager System, Collection System, Collision Detection, UI, Weaponry etc
* Version: 0.1

### How do I get set up? ###
* Using **Unity Package Manager** (*version 2018.3* or above): 
	* Open the manifest.json file inside your Unity project **Packages** folder;
	* On **dependencies**, add this line **"com.actioncode.gameplay": "https://HyagoGow@bitbucket.org/nostgameteam/gameplay.git"**

### Who do I talk to? ###
* Repo owner and admin: **Hyago Oliveira** (hyagogow@gmail.com)