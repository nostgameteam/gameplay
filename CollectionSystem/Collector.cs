﻿using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component is used to triggers collisions events with <see cref="Collectable"/> components.
    /// </summary>
    [DisallowMultipleComponent]
    public sealed class Collector : MonoBehaviour
    {
        [Tooltip("Offset of the bounding box using for detect collisions.")]
        public Vector3 offset = Vector3.zero;
        [Tooltip("Size of the bounding box using for detect collisions.")]
        public Vector3 size = Vector3.one;
        [Tooltip("Layer Mask used to narrow the bounding box collision.")]
        public LayerMask targetLayers;
        [Tooltip("Enable this with you are developing a 2D game.")]
        public bool use2DCollider = false;

        /// <summary>Callback event triggered when the TriggerCollision event happens.</summary>
        [Space]
        public CollectableEvent onCollectEvent;

        private void Reset()
        {
            Collider2D collider2D = GetComponent<Collider2D>();
            if (collider2D)
            {
                offset = collider2D.offset;
                size = collider2D.bounds.size;
            }
            else
            {
                Collider collider = GetComponent<Collider>();
                if (collider)
                {
                    offset = collider.bounds.center;
                    size = collider.bounds.size;
                }
            }
        }
        private void FixedUpdate()
        {
            if (use2DCollider)
            {
                Collider2D collider = Physics2D.OverlapBox(transform.position + offset, size, 0f, targetLayers);
                if (collider)
                {
                    Collectable collectableComponent = collider.GetComponent<Collectable>();
                    if (collectableComponent) TriggerCollision(collectableComponent);
                }
            }
            else
            {
                Collider[] colliders = Physics.OverlapBox(transform.position + offset, size / 2f, transform.rotation, targetLayers);
                if (colliders.Length > 0)
                {
                    Collectable collectableComponent = colliders[0].GetComponent<Collectable>();
                    if (collectableComponent) TriggerCollision(collectableComponent);
                }
            }
        }

        /// <summary>
        /// Triggers the collision with a given Collectable component.
        /// <para>Invokes this <see cref="onCollectEvent"/> callback and <see cref="Collectable.onCollectedEvent"/> callback.</para>
        /// </summary>
        /// <param name="collectable">The Collectable component that will be triggered.</param>
        public void TriggerCollision(Collectable collectable)
        {
            collectable.Collect(this);
            onCollectEvent.Invoke(collectable);
        }

        /// <summary>
        /// A custom UnityEvent class for providing Collectable events.
        /// </summary>
        [System.Serializable] public class CollectableEvent : UnityEvent<Collectable> { }
    }
}