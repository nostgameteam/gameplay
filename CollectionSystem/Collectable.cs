﻿using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component should be used as a base class for gameplay's collectible items such as power ups, energy capsules, potions etc.
    /// <para>You may just attach it to a GameObject and use rightway to provide a colleactable events.</para>
    /// <para>To work correctily, you need to instanciate a <see cref="Collector"/> component into your scene to be able
    /// to trigger the collision using <see cref="Collector.TriggerCollision(Collectable)"/>.</para>
    /// </summary>
    [DisallowMultipleComponent]
    public class Collectable : MonoBehaviour
    {
        [Tooltip("After the Collect event happens, this game object will be disable if true or it'll be destroyed otherwise.")]
        public bool disableAfterCollected = true;
        [Tooltip("A float value you can use.")]
        public float value = 1f;

        /// <summary>Callback event triggered when the Collect event happens.</summary>
        [Space]
        public CollectorEvent onCollectedEvent;

        private void Reset()
        {
            if (GetComponent<Collider2D>() == null && GetComponent<Collider>())
            {
                Debug.LogError("You should include a Collider2D or a Collider on this component to detect collisions!");
            }
        }

        /// <summary>
        /// The Collect event. Only executes if this component is enabled.
        /// <para>Invokes this <see cref="onCollectedEvent"/> callback using the Collector 
        /// that triggered this event and disable this component if 
        /// <see cref="disableAfterCollected"/> is true.</para>
        /// </summary>
        /// <param name="collector">The Collector component that triggered this event.</param>
        public virtual void Collect(Collector collector)
        {
            if (!this.enabled) return;

            onCollectedEvent.Invoke(collector);
            if (disableAfterCollected) gameObject.SetActive(false);
            else Destroy(gameObject);
        }

        /// <summary>
        /// A custom UnityEvent class for providing Collector events.
        /// </summary>
        [System.Serializable] public class CollectorEvent : UnityEvent<Collector> { }
    }
}