﻿using UnityEngine;
using UnityEditor;

namespace ActionCode.Gameplay.Editors
{
    [CustomEditor(typeof(PoolContainer))]
    public sealed class PoolContainerEditor : Editor
    {
        private PoolContainer _container;
        private SerializedProperty _prefabProperty;
        private SerializedProperty _instanciateOnAwakeProperty;

        private void OnEnable()
        {
            _container = (PoolContainer)target;
            _prefabProperty = serializedObject.FindProperty("_prefab");
            _instanciateOnAwakeProperty = serializedObject.FindProperty("_instanciateOnAwake");
        }

        // Draws variables into Inspector View
        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            if (PrefabUtility.IsPartOfAnyPrefab(_container))
            {
                EditorGUILayout.HelpBox("This container is part of a Prefab. Open this prefab to be able to edit this Pool Container.", MessageType.Warning);

                if (GUILayout.Button("Open Prefab"))
                {
                    string prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(_container);
                    AssetDatabase.OpenAsset(AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath));
                }
            }
            else
            {
                base.OnInspectorGUI();
                if (EditorGUI.EndChangeCheck())
                {
                    if (_instanciateOnAwakeProperty.boolValue == false)
                    {
                        GameObject prefab = _prefabProperty.objectReferenceValue as GameObject;
                        if (prefab)
                        {
                            PoolableComponent poolable = prefab.GetComponent<PoolableComponent>();
                            if (poolable == null)
                            {
                                EditorGUILayout.HelpBox(string.Format("Prefab {0} is not a Poolable Component. It'll be set to null.", prefab.name), MessageType.Error);
                                _prefabProperty.objectReferenceValue = null;
                                serializedObject.ApplyModifiedProperties();
                                return;
                            }

                            _container.gameObject.name = _container.ToString();

                            DestroyAllChildren();
                            for (int i = 0; i < _container.Amount; i++)
                            {
                                GameObject prefabInstance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
                                prefabInstance.transform.parent = _container.transform;
                                prefabInstance.transform.localPosition = Vector3.zero;
                                prefabInstance.SetActive(poolable.IsEnableOnAwake());
                            }
                        }
                        else _container.gameObject.name = "Container Is Empty";
                    }
                    else DestroyAllChildren();
                }
            }
        }

        private void DestroyAllChildren()
        {
            if (_container.transform.childCount == 0) return;
            foreach (Transform child in _container.gameObject.GetChildren(true))
            {
                DestroyImmediate(child.gameObject, true);
            }
        }
    }
}