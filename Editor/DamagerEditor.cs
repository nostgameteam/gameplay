﻿using UnityEditor;
using UnityEngine;
using UnityEditor.IMGUI.Controls;

namespace ActionCode.Gameplay.Editors
{
    [CustomEditor(typeof(Damager))]
    public sealed class DamagerEditor : Editor
    {
    	private Damager _damager;
        private BoxBoundsHandle _boxBoundsHandle = new BoxBoundsHandle();
        private readonly Color _ENABLED_COLOR = Color.green + Color.grey;

        private void OnEnable()
        {
            _damager = (Damager) target;
        }
        private void OnSceneGUI()
        {
            // draws a box handle similar a box colider component (2D or 3D)
            Matrix4x4 handleMatrix = _damager.transform.localToWorldMatrix;
            handleMatrix.SetRow(0, Vector4.Scale(handleMatrix.GetRow(0), new Vector4(1f, 1f, 0f, 1f)));
            handleMatrix.SetRow(1, Vector4.Scale(handleMatrix.GetRow(1), new Vector4(1f, 1f, 0f, 1f)));
            handleMatrix.SetRow(2, new Vector4(0f, 0f, 1f, _damager.transform.position.z));
            using (new Handles.DrawingScope(handleMatrix))
            {
                if (_damager.use2DCollider)
                {
                    _boxBoundsHandle.center = _damager.offset.ToVector2();
                    _boxBoundsHandle.size = _damager.size.ToVector2();
                }
                else
                {
                    _boxBoundsHandle.center = _damager.offset;
                    _boxBoundsHandle.size = _damager.size;
                }

                _boxBoundsHandle.SetColor(_ENABLED_COLOR);
                EditorGUI.BeginChangeCheck();
                _boxBoundsHandle.DrawHandle();
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(_damager, "Modify Damager Collider");

                    _damager.size = _boxBoundsHandle.size;
                    _damager.offset = _boxBoundsHandle.center;
                }
            }
        }
    }
}