using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    [DisallowMultipleComponent]
    public abstract class AbstractCollisionDetector : MonoBehaviour
    {
        [Header("Events")]
        public UnityEvent onEnterEvent;
        public UnityEvent onStayEvent;
        public UnityEvent onExitEvent;

        [Header("Params")]
        public LayerMask targetLayers;
        public float distance = 5f;
        [SerializeField] private Vector3 _defaultForwardDirection = Vector3.left;

        public Transform CollisionTarget { get; protected set; }

        private readonly Color COLLISION_COLOR = new Color(1f, 0f, 0f, 0.2f);
        private readonly Color NO_COLLISION_COLOR = new Color(0f, 1f, 0f, 0.2f);

        private void Update()
        {
            // checks if CollisionTarget has some data before clear it
            bool wasColliding = CollisionTarget != null;
            CollisionTarget = null;
            UpdateCollisionDetection();

            // has a current target collision
            if (CollisionTarget != null)
            {
                // if was not colliding, invoke the on enter event if avaliable
                if (!wasColliding) onEnterEvent.Invoke();
                else onStayEvent.Invoke();
            }
            //has no current target collision and was colliding, invoke the on exit event if avaliable
            else if (wasColliding) onExitEvent.Invoke();
        }

        private void OnDrawGizmosSelected()
        {
            UpdateCollisionGizmos();
        }

        protected abstract void UpdateCollisionDetection();
        protected abstract void UpdateCollisionGizmos();

        protected Color GetGizmosColor()
        {
            return CollisionTarget != null ? COLLISION_COLOR : NO_COLLISION_COLOR;
        }

        public Vector3 GetForwardDirection()
        {
            return transform.rotation * _defaultForwardDirection;
        }
    }
}