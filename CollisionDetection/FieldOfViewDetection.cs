using UnityEngine;

namespace ActionCode.Gameplay
{
    public class FieldOfViewDetection : AbstractCollisionDetector
    {
        [Range(0f, 360f)] public float angle = 45f;

        protected override void UpdateCollisionDetection()
        {
            // checks first for any collider inside the circle radius
            Collider2D cicleCollider = Physics2D.OverlapCircle(transform.position, distance, targetLayers);

            // if so, checks if this collider is inside the field of view angle
            if (cicleCollider != null)
            {
                // if the angle is close to 360, no need for more calculations
                if (angle > 355f) CollisionTarget = cicleCollider.transform;
                else
                {
                    Vector3 forwardDirection = GetForwardDirection();
                    Vector3 finalPosition = transform.position + forwardDirection * distance;
                    Vector3 closestCollisionPoint = cicleCollider.bounds.ClosestPoint(finalPosition);
                    Vector3 dirToTarget = (closestCollisionPoint - transform.position).normalized;
                    float angleToTarget = Vector3.Angle(dirToTarget, forwardDirection);
                    if (angleToTarget < angle * 0.5f) CollisionTarget = cicleCollider.transform;
                }
            }
        }

        protected override void UpdateCollisionGizmos()
        {
#if UNITY_EDITOR
            Vector3 arcFrom = Quaternion.Euler(0, 0, angle * 0.5f) * GetForwardDirection();
            UnityEditor.Handles.color = GetGizmosColor();
            UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.back, arcFrom, angle, distance);
#endif
        }
    }
}