﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// Put this component inside a Game Object with a Particle System to be able to use it with a <see cref="PoolContainer"/>.
    /// <para>Particle System with play on awake enabled will not have its GameObject disabled. This is for optimization.</para>
    /// </summary>
    [RequireComponent(typeof(ParticleSystem))]
    public sealed class PoolableParticleSystem : PoolableComponent
    {
        public ParticleSystem partSystem;
        public AudioSource audioSource;

        private void Reset()
        {
            partSystem = GetComponent<ParticleSystem>();
            audioSource = GetComponent<AudioSource>();

            timeAlive = partSystem.main.duration;
            disableWhenBack = partSystem.main.playOnAwake;
        }

        internal override void Enable()
        {
            base.Enable();
            partSystem.Play();
            if (audioSource) audioSource.Play();
        }
        internal override void Disable()
        {
            base.Disable();
            if (partSystem.isPlaying) partSystem.Pause();
            else partSystem.Stop();

            if (audioSource && audioSource.isPlaying) audioSource.Stop();
        }
    }
}