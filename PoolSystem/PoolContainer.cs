using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// Pool Container for Poolable components.
    /// <para>To work correctly, put inside Prefab any Game Object with a <see cref="PoolableComponent"/>.</para>
    /// </summary>
    public sealed class PoolContainer : MonoBehaviour
    {
        [SerializeField, Tooltip("Put here any Game Object with a PoolableComponent on it.")]
        private GameObject _prefab;
        [SerializeField, Tooltip("Should the prefabs be instanciated only on Awake?")]
        private bool _instanciateOnAwake = true;
        [SerializeField, Tooltip("Max amount for this pool container.")]
        private int _amount = 5;
        [Tooltip("Prefabs out of the pool will be placed as child of this transform. Use it for scene organization.")]
        public Transform placedParent;

        /// <summary>Max amount for this pool container.</summary>
        public int Amount { get { return _amount; } }
        /// <summary>Current avaliable amount for this pool container.</summary>
        public int AmountAvaliable { get; private set; }

        private PoolableComponent[] _poolableComponents;

        private void Awake()
        {
            if (_instanciateOnAwake)
            {
                _poolableComponents = new PoolableComponent[_amount];
                for (int i = 0; i < _poolableComponents.Length; i++)
                {
                    _poolableComponents[i] = Instantiate(_prefab, transform).GetComponent<PoolableComponent>();
                    _poolableComponents[i].InitContainer(this, i);
                }
            }
            else
            {
                _poolableComponents = GetComponentsInChildren<PoolableComponent>(true);
                for (int i = 0; i < _poolableComponents.Length; i++)
                {
                    _poolableComponents[i].InitContainer(this, i);
                }
            }

            AmountAvaliable = _poolableComponents.Length;
        }

        /// <summary>
        /// Places and returns the casted poolable component at the given position.
        /// </summary>
        /// <typeparam name="T">The casted Poolable Component instance.</typeparam>
        /// <param name="position">The position to place the poolable component.</param>
        /// <returns>The casted Poolable Component instance.</returns>
        public T Place<T>(Vector3 position) where T : PoolableComponent
        {
            return (T)Place(position);
        }

        /// <summary>
        /// Places the poolable component at the given position.
        /// </summary>
        /// <param name="position">The position to place the poolable component.</param>
        /// <returns>The Poolable Component instance.</returns>
        public PoolableComponent Place(Vector3 position)
        {
            return Place(position, _prefab.transform.rotation, _prefab.transform.localScale, placedParent);
        }

        /// <summary>
        /// Places the poolable component at the given position and rotation.
        /// </summary>
        /// <param name="position">The position to place the poolable component.</param>
        /// <param name="rotation">The rotation to place the poolable component.</param>
        /// <returns>The Poolable Component instance.</returns>
        public PoolableComponent Place(Vector3 position, Quaternion rotation)
        {
            return Place(position, rotation, _prefab.transform.localScale, placedParent);
        }

        /// <summary>
        /// Places the poolable component at the given position, rotation and scale.
        /// </summary>
        /// <param name="position">The position to place the poolable component.</param>
        /// <param name="rotation">The rotation to place the poolable component.</param>
        /// <param name="scale">The scale to place the poolable component.</param>
        /// <param name="parent">Place the poolable component as a child if specified.</param>
        /// <returns>The Poolable Component instance.</returns>
        public PoolableComponent Place(Vector3 position, Quaternion rotation, Vector3 scale, Transform parent = null)
        {
            for (int i = 0; i < _poolableComponents.Length; i++)
            {
                if (_poolableComponents[i].Available)
                {
                    _poolableComponents[i].Place(position, rotation, scale, parent);
                    _poolableComponents[i].Enable();
                    AmountAvaliable--;
                    return _poolableComponents[i];
                }
            }

            Debug.LogWarningFormat("There are no more {0} avaliable at {1}. Increase Amount to be able to create more.", 
                GetPoolablePrefabName(), ToString());
            return null;
        }

        /// <summary>
        /// Returns if this container is empty.
        /// </summary>
        /// <returns>True if this container is empty.</returns>
        public bool IsEmpty()
        {
            return AmountAvaliable == 0;
        }

        public override string ToString()
        {
            return string.Format("Container [{0}]", GetPoolablePrefabName());
        }
        public string GetPoolablePrefabName()
        {
            return _prefab ? _prefab.name : "";
        }

        internal void TurnBack(int index)
        {
            _poolableComponents[index].Place(Vector3.zero, _prefab.transform.rotation, _prefab.transform.localScale, transform);

            AmountAvaliable++;
        }

        private void OnValidate()
        {
            _amount = Mathf.Clamp(_amount, 1, 10000);
        }
    }
}