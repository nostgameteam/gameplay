﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// Base class for a component able to be put inside a <see cref="PoolContainer"/>.
    /// </summary>
    [DisallowMultipleComponent]
    public class PoolableComponent : MonoBehaviour
    {
        [Min(0f), Tooltip("Time, in seconds, this component will be out of the pull before send back.")]
        public float timeAlive = 1f;
        [Tooltip("Should disable this Game Object when back to the pool?")]
        public bool disableWhenBack = true;

        public float CurrentEnabledTime { get; protected set; }
        public bool Available { get; protected set; }

        private int _poolIndex;
        private PoolContainer _container;

        protected virtual void Start()
        {
            CurrentEnabledTime = 0f;
        }
        protected virtual void Update()
        {
            UpdateEnableTime();
        }
        protected virtual void UpdateEnableTime()
        {
            CurrentEnabledTime += Time.deltaTime;
            if (CurrentEnabledTime > timeAlive) Disable();
        }
        public virtual bool IsEnableOnAwake()
        {
            return !disableWhenBack;
        }

        internal virtual void Enable()
        {
            CurrentEnabledTime = 0f;
            Available = false;
            gameObject.SetActive(enabled = true);
        }
        internal virtual void Disable()
        {
            CurrentEnabledTime = 0f;
            if (_container) _container.TurnBack(_poolIndex);
            Available = true;
            gameObject.SetActive(!disableWhenBack);
        }
        internal void InitContainer(PoolContainer container, int poolIndex)
        {
            Available = true;
            _container = container;
            _poolIndex = poolIndex;

            gameObject.name += "-" + _poolIndex.ToString("d2");
            gameObject.SetActive(IsEnableOnAwake());
        }       
        internal void Place(Vector3 position, Quaternion rotation, Vector3 scale, Transform parent = null)
        {
            transform.parent = null;
            transform.localPosition = position;
            transform.localScale = scale;
            transform.localRotation = rotation;
            transform.parent = parent;
        }
    }
}