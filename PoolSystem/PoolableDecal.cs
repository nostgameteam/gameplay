﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    public sealed class PoolableDecal : PoolableComponent
    {
        [Range(0f, 10f)] public FloatPair sizeRange = new FloatPair(1f, 5f);
        [Range(-360, 360)] public IntegerPair rotationRange = new IntegerPair(-180, 180);
        [Range(0.1f, 1f)] public float fadingSpeed = .5f;
        public LayerMask _collisionMask = -1;

        private float _inicialAlpha;
        private bool _fading;
        private Material _material;

        protected override void Start()
        {
            base.Start();
            _material = GetComponent<Renderer>().material;
            _inicialAlpha = _material.color.a;
        }
        protected override void Update()
        {
            if (_fading)
            {
                Color color = _material.color;
                color.a -= Time.deltaTime * fadingSpeed;
                _material.color = color;
                if (color.a < 0.1f) Disable();
            }
            else
            {
                CurrentEnabledTime += Time.deltaTime;
                _fading = CurrentEnabledTime > timeAlive;
            }
        }

        private void OnEnable()
        {
            _fading = false;
            Color color = _material.color;
            color.a = _inicialAlpha;
            _material.color = color;

            transform.localScale = Vector3.one * sizeRange.RandomValue;

            RaycastHit hit;
            if (Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, 10f, _collisionMask, QueryTriggerInteraction.Ignore))
            {
                transform.position = hit.point + hit.normal * .01f;
                transform.rotation = Quaternion.LookRotation(hit.normal) * Quaternion.AngleAxis(rotationRange.RandomValue, Vector3.forward);
            }
        }
    }
}