using UnityEngine;
using UnityEngine.Events;

namespace ActionCode.Gameplay
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(AudioSource))]
    public class MuzzleFlash : MonoBehaviour
    {
        public float time = 0.2f;
        public new Light light;
        public AudioSource audioSource;
        public AbstractAnimation flashAnimation;
        public AbstractAnimation smokeAnimation;
        public ParticleSystem flashParticleSystem;
        public ParticleSystem smokeParticleSystem;

        protected UnityEvent onShowEvent;
        protected UnityEvent onHideEvent;

        private void Reset()
        {
            light = GetComponentInChildren<Light>(true);
            flashAnimation = GetComponent<AbstractAnimation>();
            flashParticleSystem = GetComponent<ParticleSystem>();
            audioSource = GetComponent<AudioSource>();
            audioSource.ResetSetup();
        }
        private void Awake()
        {
            InitEvents();
        }

        protected virtual void InitEvents()
        {
            onShowEvent = new UnityEvent();
            onHideEvent = new UnityEvent();

            if (light)
            {
                onShowEvent.AddListener(ShowLight);
                onHideEvent.AddListener(HideLight);
            }
            if (flashAnimation)
            {
                onShowEvent.AddListener(flashAnimation.Replay);
                onHideEvent.AddListener(flashAnimation.Stop);
            }
            if (smokeAnimation)
            {
                onShowEvent.AddListener(smokeAnimation.Replay);
                onHideEvent.AddListener(smokeAnimation.Stop);
            }
            if (audioSource.clip) onShowEvent.AddListener(audioSource.Play);
            if (flashParticleSystem) onShowEvent.AddListener(flashParticleSystem.Play);
            if (smokeParticleSystem) onShowEvent.AddListener(smokeParticleSystem.Play);
        }

        public void Show()
        {
            onShowEvent.Invoke();
            CancelInvoke("Hide");
            Invoke("Hide", time);
        }
        public void Hide()
        {
            onHideEvent.Invoke();
        }

        private void ShowLight()
        {
            light.enabled = true;
        }
        private void HideLight()
        {
            light.enabled = false;
        }

        private void OnValidate()
        {
            time = Mathf.Abs(time);
            if (flashAnimation) flashAnimation.speed = 1f / time;
        }
    }
}