﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// This component represents a physical bullet for gameplay.
    /// <para>Its <see cref="Damager"/> components contains the damager information.</para>
    /// </summary>
    [RequireComponent(typeof(Damager))]
    public class Bullet : PoolableComponent
    {
        public float speed = 5f;
        public bool destroyOnHit = true;
        public Vector3 direction = Vector3.right;

        private void Awake()
        {
            if (destroyOnHit)
            {
                Damager damager = GetComponent<Damager>();
                damager.onHitEvent.AddListener(Disable);
            }
        }

        protected override void Update()
        {
            UpdatePosition();
            base.Update();
        }

        public void Shoot(Vector3 direction)
        {
            this.direction = direction;
        }

        protected virtual void UpdatePosition()
        {
            transform.position += direction * speed * Time.deltaTime;
        }
    }
}