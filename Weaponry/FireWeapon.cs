﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    /// <summary>
    /// Fire Weapon component. Can shhot bullets using a given Pool System Container.
    /// </summary>
    public class FireWeapon : MonoBehaviour
    {
        [Tooltip("Bullets will be fired from this transform. If none, this transform will be used.")]
        public Transform bulletSource;
        [Required, Tooltip("Specify a Pool Container to generate bullets.")]
        public PoolContainer bulletContainer;
        [Tooltip("Specify a Pool Container to generate bullets impacts.")]
        public PoolContainer bulletImpactContainer;
        [Tooltip("Specify a bullet muzzle flash effect.")]
        public MuzzleFlash muzzleFlash;
        [Range(0, 10), Tooltip("Specify how many shots per seconds.")]
        public int shotsPerSeconds = 6;

        public float LastShootTime { get; private set; }


        protected virtual void Reset()
        {
            bulletSource = transform.FindContaining("BulletSource");
            muzzleFlash = GetComponentInChildren<MuzzleFlash>(true);
        }

        private void Awake()
        {
            LastShootTime = 0f;
        }

        public bool CanShoot()
        {
            float timeSinceLastFire = Time.timeSinceLevelLoad - LastShootTime;
            return timeSinceLastFire - 1f / shotsPerSeconds > 0f;
        }

        /// <summary>
        /// Shoots a bullet forward using <see cref="bulletSource"/>.
        /// </summary>
        public void Shoot()
        {
            Shoot(bulletSource.forward);
        }

        /// <summary>
        /// Shoots a bullet using the given direction.
        /// </summary>
        /// <param name="direction">The direction where the bullet will be shoot.</param>
        public void Shoot(Vector3 direction)
        {
            if (bulletSource) Shoot(bulletSource.position, direction);
            else Shoot(transform.position, direction);
        }

        /// <summary>
        /// Shoots a bullet using the given position and direction.
        /// </summary>
        /// <param name="position">The position where the bullet will be shoot.</param>
        /// <param name="direction">The direction where the bullet will be shoot.</param>
        public void Shoot(Vector3 position, Vector3 direction)
        {
            if (bulletContainer.IsEmpty()) ShowNoMunitionMessage();
            else if (CanShoot())
            {
                Bullet bullet = bulletContainer.Place<Bullet>(position);
                if (bullet)
                {
                    bullet.Shoot(direction);
                    if (muzzleFlash) muzzleFlash.Show();
                    LastShootTime = Time.timeSinceLevelLoad;
                }                
            }            
        }

        /// <summary>
        /// Places a bullet impact at given position using <see cref="bulletImpactContainer"/>.
        /// </summary>
        /// <param name="position">The position where the bullet impact will be placed.</param>
        public void PlaceBulletImpact(Vector3 position)
        {
            if (bulletImpactContainer) bulletImpactContainer.Place(position);
        }

        public override string ToString()
        {
            return string.Format("Weapon: {0}.", gameObject.name);
        }

        protected virtual void ShowNoMunitionMessage()
        {
            Debug.Log("No munition in " + this);
        }
    }
}