﻿using UnityEngine;
using System.Collections;

namespace ActionCode.Gameplay
{
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Camera))]
    public class CameraShaker : MonoBehaviour
    {
        private Vector3 _originalPos;

        private void Awake()
        {
            _originalPos = transform.position;
        }

        /// <summary>
        /// Shakes the camera with the given duration, horizontal and vertical magnitudes.
        /// </summary>
        /// <param name="duration">Shake duration.</param>
        /// <param name="horMagnitude">Horizontal Magnitude. How widely will be the shaking on the horizontal axis.</param>
        /// <param name="verMagnitude">Vertical Magnitude. How widely will be the shaking on the vertical axis.</param>
        public void Shake(float horMagnitude = 0.1f, float verMagnitude = 0.1f, float duration = 0.25f)
        {
            StartCoroutine(ShakeCoroutine(duration, horMagnitude, verMagnitude));
        }

        /// <summary>
        /// Stops the camera from shaking.
        /// </summary>
        public void Stop()
        {
            StopAllCoroutines();
            transform.position = _originalPos;
        }

        private IEnumerator ShakeCoroutine(float duration, float hMag, float vMag)
        {
            _originalPos = transform.position;
            float time = 0f;
            while (time < duration)
            {
                Vector3 offset = new Vector2(
                    Random.Range(-1f, 1f) * hMag,
                    Random.Range(-1f, 1f) * vMag);

                transform.position = _originalPos + offset;
                time += Time.deltaTime;
                yield return null;
            }

            transform.position = _originalPos;
        }
    }
}