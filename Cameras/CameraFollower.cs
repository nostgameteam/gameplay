﻿using UnityEngine;

namespace ActionCode.Gameplay
{
    [ExecuteInEditMode]
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Camera))]
    public sealed class CameraFollower : MonoBehaviour
    {
        [Tooltip("Camera will follow this transform.")] public Transform target;
        public Vector3 offset = Vector3.zero;
        [Tooltip("Follow Traget on X axis.")] public bool followX = true;
        [Tooltip("Follow Traget on Y axis.")] public bool followY = true;
        [Tooltip("Follow Traget on Z axis.")] public bool followZ = true;

        private void Reset()
        {
            FindTarget();
        }
        private void Start()
        {
            if (target == null) FindTarget();
        }
        private void LateUpdate()
        {
            Vector3 pos = transform.position;
            if (followX) pos.x = target.position.x + offset.x;
            if (followY) pos.y = target.position.y + offset.y;
            if (followZ) pos.z = target.position.z + offset.z;

            transform.position = pos;
        }

        [ContextMenu("Find Target")]
        public void FindTarget()
        {
            GameObject player = GameObject.FindWithTag("Player");
            if (player) target = player.transform;
        }
    }
}